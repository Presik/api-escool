
# TRYTON ECOMMERCE API

Esta guia describe los métodos para conectarse a la API de Tryton, creada por Presik Techonologies SAS.


## Endpoints


A toda petición deben agregar el token para que las request sean válidas.

Los Endpoints son las rutas para conectarse a la API, las rutas son:


A toda peticion se le debe pasar la api_key:

  args['headers']['authorization'] = api_key


### Datos Geograficos


#### Consulta Paises

Esta ruta retorna el listado de paises:

```
method: GET

uri: /DB/countries

```

#### Consulta Departamentos

Esta ruta retorna el listado de departamentos:

```
method: GET

uri: /DB/departments

```


#### Consulta Ciudades

Esta ruta retorna el listado de ciudades por departamento:

```
method: GET

uri: /DB/cities/:departmentId

```

### Terceros


#### Consulta

Esta ruta retorna el listado de terceros (clientes) que esten registrados en la base de datos:

```
method: GET

uri: /DB/parties

```

#### Consulta

Esta ruta toma el numero de identificación del cliente y devuelve los datos del mismo
si esta registrado en la base de datos, en caso contrario retorna Null:

```
method: GET

uri: /DB/party/:idNumber

```


#### Creación

Esta ruta permite crear un tercero:

```
method: POST

uri: /DB/add_party

body: {object}

```

Los campos Object son:

* name: Nombre - Cadena
* id_number: Numero de identificacion - Alfaumerico
* email: Correo - Cadena
* address: Dirección - Cadena
* phone: Telefono - Alfanumerico
* type_document: Tipo de documento - Cadena de 2 dígitos
    ⋅⋅⋅ 12 > Tarjeta de Identidad
    ⋅⋅⋅ 13 > Cedula de Ciudadania
    ⋅⋅⋅ 22 > Cedula de Extranjeria,
    ⋅⋅⋅ 31 > NIT
    ⋅⋅⋅ 41 > Pasaporte
* city: Ciudad - Entero (Id de la ciudad)
* country: Pais - Entero (Id del país)
* department: Departamento - Entero (Id del departamento)


El id_number debe ser sin puntos y sin comas



#### Actualización

Esta ruta permite actualizar los datos de un tercero:

```
method: PUT, POST

uri: /DB/update_party

body: {object}

```

Object pueden ser los mismos campos usados en la creación, pero adicionalmente
se debe pasar el id del cliente, para que se pueda actualizar el registro.

Ejemplo:

 {
   id: 4,
   phone: '+57 30424682'
   name: 'JHON SMITH',
   id_number: '472862753',
   type_document: '13',  // Options: '13' - '31' ver arriba ...
   address: 'AVE 46 N 36 109',
   country: 48,
   department: 28,
   city: 848,
   phone: '5938675',
   email: 'prueba@gmail.com'
 }


### Productos


#### Consulta por Tienda

Esta ruta permite obtener los productos disponibles para la venta, con el inventario actual, para la tienda señalada:


```
method: GET

uri: /DB/products_by_shop/:shopId

```


#### Consulta por Categorias y Tienda

Esta ruta permite obtener los productos disponibles para la tienda, con el Id de su categoria y su inventario actual:


```
method: GET

uri: /DB/categorized_products/:shopId

```

#### Consulta por Categorias en Jerarquia por Tienda

Esta ruta permite obtener las categorias de la tienda en una estructura de arbol o jerarquica:


```
method: GET

uri: /DB/categories_tree/:shopId

```



#### Consulta de Prooductos por Tienda y Categoria

Esta ruta permite obtener los productos disponibles para una tienda con su Id y
por una categoria usando el Id y su inventario actual:


```
method: GET

uri: /DB/products_shop_category/:shopId/:categoryId

```

#### Busqueda de Productos por Nombre

Esta ruta permite buscar los productos por su cualquier combinacion del nombre,
indistinto de mayúsculas y minúsculas:


```
method: GET

uri: /DB/products_elastic/:shopId/:query

```

query : Texto del producto a buscar


### Tiendas


#### Consulta

Esta ruta permite obtener el listado de tiendas con su Id:


```
method: GET

uri: /DB/shops

```


### Ventas


#### Consulta de Formas de Pago

Esta ruta permite obtener las formas de pago con su Id:


```
method: GET

uri: /DB/payment_term

```


#### Creación

Esta ruta permite crear una venta-factura y retorna el estado de la transacción:

```
method: POST

uri: /DB/add_sale

body: {object}

```

Los campos Object son:

* party: Id del tercero - Entero
* shop: Id de la tienda - Entero
* payment_term: Id de forma de pago - Entero
* lines: lineas de producto - Array de objetos
    + product: id de producto - Entero
    + quantity: cantidad - Entero
    + list_price: precio unitario - Cadena (Ej: '23400.78') max 4 decimales
* paid_amount: Valor del pago de la venta - Cadena (Ej: '23400.78') max 2 decimales
* voucher: Numero del comprobante de pago - Cadena

Nota: incluir en las lineas el flete, como un producto mas.


### Flete


#### Consulta valor flete

Esta ruta permite obtener el id del producto flete y el valor del mismo:


```
method: GET

uri: /DB/product_freight/

```
