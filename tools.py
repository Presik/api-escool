# Multiple tools for psk api
import os
import time
import base64
import logging
import types
import configparser
from datetime import date

from Crypto.Cipher import XOR
from flask import jsonify, request
from flask.json import JSONEncoder
import math
import simplejson as json


def get_config(param=None):
    default_dir = os.path.join(os.getenv('HOME'), '.flask')
    config_file = os.path.join(default_dir, 'escool.ini')

    config = configparser.ConfigParser()
    config.read(config_file)
    if param:
        return config.get('General', param)
    return config


def encrypt(key, plaintext):
    cipher = XOR.new(key)
    return base64.b64encode(cipher.encrypt(plaintext))


def send(data):
    res = jsonify(data)
    res.headers.add('Access-Control-Allow-Origin', '*')
    res.headers.add('Access-Control-Allow-Methods', 'POST, GET, PUT, OPTIONS')
    res.headers.add('Access-Control-Allow-Headers', 'Origin, Content-Type, X-Auth-Token')
    return res


def print_rec(rec):
    for k, j in rec.items():
        print(k, ' : ', j)


def is_class(obj):
    if obj.get('__class__'):
        # FIXME: Add datetime, bytes, decimal, too
        if obj['__class__'] == 'date':
            return date(obj['year'], obj['month'], obj['day'])
    return obj


def get_data(decode=False):
    if request.args:
        # From react web
        data = request.args.to_dict()
        if data.get('context'):
            data['context'] = eval(data['context'])
        return data
    elif hasattr(request, 'data') and request.data != b'':
        data = request.data.decode("utf-8")
        return json.loads(data, object_hook=is_class)
    else:
        try:
            return request.json
        except AttributeError:
            logging.warning('Attribute error unknown...!')


def truncate(number, digits) -> float:
    stepper = 10.0 ** digits
    return math.trunc(stepper * number) / stepper


def jsonify_record(record, fields=None, binaries=None):
    res_fields = {}
    if not fields:
        fields = ['id', 'name', 'rec_name']
    else:
        fields.append('rec_name')
    for fd in fields:
        if '.' in fd:
            target = fd.split('.')
            _field = target.pop(0)
            next_field = '.'.join(target)
            if _field not in res_fields.keys():
                res_fields[_field] = []
            res_fields[_field].append(next_field)
        else:
            res_fields[fd] = []

    _record_data = {'id': record.id}

    for f in res_fields:
        try:
            value = getattr(record, f, None)
        except:
            logging.warning('Attribute field unknown > ', f, record.__name__)
            _record_data[f] = None
            continue
        if value is None:
            _record_data[f] = None
            continue
        elif hasattr(value, 'id'):
            _record_data[f] = {
                'id': value.id
            }
            if hasattr(value, 'rec_name'):
                _record_data[f]['name'] = value.rec_name
            elif hasattr(value, 'name'):
                _record_data[f]['name'] = value.name

            child_fields = res_fields.get(f)
            if child_fields:
                _record_data[f] = jsonify_record(value, child_fields)
        elif value and isinstance(value, tuple):
            try:
                sub_model = value[0].__name__
                sub_fields = _models[sub_model]['fields']
                sub_elements = [(jsonify_record(e, sub_fields)) for e in value]
                _record_data[f] = sub_elements
            except:
                sub_fields = res_fields[f]
                sub_elements = [(jsonify_record(v, sub_fields)) for v in value]
                _record_data[f] = sub_elements
        elif value and isinstance(value, list):
            # Flask require that lists are dumped before send
            _record_data[f] = json.dumps(value)
        elif value and isinstance(value, time):
            # Flask require that lists are dumped before send
            _record_data[f] = str(value)
        else:
            _record_data[f] = value

    if binaries:
        for fb in binaries:
            val = getattr(record, fb, None)
            if val:
                _record_data[fb] = base64.b64encode(val)
            else:
                _record_data[fb] = None
    return _record_data


class InvalidAPIUsage(Exception):
    status_code = 400

    def __init__(self, message, status_code=None, payload=None):
        super().__init__()
        self.message = message
        if status_code is not None:
            self.status_code = status_code
        self.payload = payload

    def to_dict(self):
        rv = dict(self.payload or ())
        rv['message'] = self.message
        return rv


class CustomJSONEncoder(JSONEncoder):

    def default(self, obj):
        try:
            if isinstance(obj, types.ModuleType):
                return str(obj)
            elif isinstance(obj, types.MethodType):
                return str(obj)
            elif isinstance(obj, date):
                return obj.isoformat()
            elif isinstance(obj, 'bytes'):
                return base64.b64encode(obj)
            iterable = iter(obj)
        except TypeError:
            pass
        else:
            return list(iterable)
        return JSONEncoder.default(self, obj)
